# Kubernetes

Kubernetes project to run a Nginx pod (port 80) with network connection with another pod running MySQL.

### Running the project

This project was built using Minikube.

The `01-simple` folder is the easiest solution, the simpler one.

The `02-deployments-v2` folder consists on a more "production-ready" approach, using services and deployments.
